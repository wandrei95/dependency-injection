package com.andrei.wegroszta.depinj.spellchecker;

import com.andrei.wegroszta.depinj.texteditor.SpellChecker;

public class LowercaseSpellChecker implements SpellChecker {
    @Override
    public boolean isTextValid(String text) {
        return text.toLowerCase().equals(text);
    }
}
