package com.andrei.wegroszta.depinj.spellchecker;

public interface SpellCheckerLogger {
    void logMessage(String message);
}
