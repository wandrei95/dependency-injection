package com.andrei.wegroszta.depinj.spellchecker;

import com.andrei.wegroszta.depinj.texteditor.SpellChecker;

public class LengthSpellChecker implements SpellChecker {
    private SpellCheckerLogger logger;

    public LengthSpellChecker() {

    }

    public LengthSpellChecker(SpellCheckerLogger logger) {
        this.logger = logger;
    }

    @Override
    public boolean isTextValid(String text) {
        boolean b = text.length() < 100;

        if (b) {
            logger.logMessage("INFO: message is within the rage of 100 characters");
        } else {
            logger.logMessage("ERROR: message contains more than 100 characters");
        }

        return b;
    }
}
