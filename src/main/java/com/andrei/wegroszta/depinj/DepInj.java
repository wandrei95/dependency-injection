package com.andrei.wegroszta.depinj;

import com.andrei.wegroszta.depinj.entities.Dependency;
import com.andrei.wegroszta.depinj.entities.InnerDependency;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class DepInj {
    private final List<Dependency> dependencies;

    public DepInj() {
        dependencies = new ArrayList<>();
    }

    public void loadDataFromInputStream(InputStream inputStream) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            Type listType = new TypeToken<List<Dependency>>() {
            }.getType();
            dependencies.addAll(new Gson().fromJson(br, listType));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * all class names must be the fully qualified class names
     */
    public void addDependency(Dependency dependency) {
        verifyDependencyValidity(dependency);

        dependencies.add(dependency);
    }

    private void verifyDependencyValidity(Dependency dependency) {
        verifyClassNameValidity(dependency.getClassName());
        verifyClassNameValidity(dependency.getImplClassName());

        verifyInnerDependencies(dependency);
    }

    private void verifyInnerDependencies(Dependency dependency) {
        if (dependency.getInnerDependencies() != null) {
            for (InnerDependency innerDependency : dependency.getInnerDependencies()) {
                verifyClassNameValidity(innerDependency.getClassName());
            }
        }
    }

    private void verifyClassNameValidity(String className) {
        try {
            Class.forName(className);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(new ClassCastException("invalid class " + className));
        }
    }

    public <T> T getInstance(Class<T> cls) throws Throwable {
        Dependency foundDependency = getDependencyIfExists(cls);

        Class<?> implClass = Class.forName(foundDependency.getImplClassName());

        Object impl = instantiateImplClass(implClass);

        for (InnerDependency innerDep : foundDependency.getInnerDependencies()) {
            Class<?> fieldClass = Class.forName(innerDep.getClassName());
            Field field = implClass.getDeclaredField(innerDep.getFieldName());

            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            Object o = getInstance(fieldClass);
            field.set(impl, o);
            field.setAccessible(accessible);
        }

        return (T) impl;
    }

    private Object instantiateImplClass(Class<?> implClass) {
        Object impl;
        try {
            impl = implClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Class " + implClass.getCanonicalName() +
                    " is not instantiable and/or doesn't have a public empty constructor", e);
        }
        return impl;
    }

    private <T> Dependency getDependencyIfExists(Class<T> cls) throws Throwable {
        Optional<Dependency> optionalFoundDependency = dependencies.stream()
                .filter(dependency -> dependency.getClassName().equals(cls.getCanonicalName()))
                .findAny();

        return optionalFoundDependency
                .orElseThrow((Supplier<Throwable>) () ->
                        new RuntimeException("No registered dependency for " + cls.getCanonicalName()));
    }
}
