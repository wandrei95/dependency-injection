package com.andrei.wegroszta.depinj.entities;

public class InnerDependency {
    private final String className;
    private final String fieldName;

    public InnerDependency(String className, String fieldName) {
        this.className = className;
        this.fieldName = fieldName;
    }

    public String getClassName() {
        return className;
    }

    public String getFieldName() {
        return fieldName;
    }
}
