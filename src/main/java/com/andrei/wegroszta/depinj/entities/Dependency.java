package com.andrei.wegroszta.depinj.entities;

import java.util.ArrayList;
import java.util.List;

public class Dependency {
    private final String className;
    private final String implClassName;
    private final List<InnerDependency> innerDependencies;

    public Dependency(String className, String implClassName, List<InnerDependency> innerDependencies) {
        this.className = className;
        this.implClassName = implClassName;
        this.innerDependencies = new ArrayList<>();
        if (innerDependencies != null) {
            this.innerDependencies.addAll(innerDependencies);
        }
    }

    public String getClassName() {
        return className;
    }

    public String getImplClassName() {
        return implClassName;
    }

    public List<InnerDependency> getInnerDependencies() {
        if (innerDependencies == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(innerDependencies);
    }
}
