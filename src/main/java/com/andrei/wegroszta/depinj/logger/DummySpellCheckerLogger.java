package com.andrei.wegroszta.depinj.logger;

import com.andrei.wegroszta.depinj.spellchecker.SpellCheckerLogger;

public class DummySpellCheckerLogger implements SpellCheckerLogger {
    @Override
    public void logMessage(String message) {
        System.out.println(message);
    }
}
