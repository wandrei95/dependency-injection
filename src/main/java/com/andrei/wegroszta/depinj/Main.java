package com.andrei.wegroszta.depinj;

import com.andrei.wegroszta.depinj.texteditor.TextEditor;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws Throwable {
        ClassLoader classLoader = Main.class.getClassLoader();
        File file = new File(classLoader.getResource("texteditor.json").getFile());
        InputStream inputStream = new FileInputStream(file);

        DepInj depInj = new DepInj();
        depInj.loadDataFromInputStream(inputStream);
        TextEditor textEditor = depInj.getInstance(TextEditor.class);
        textEditor.enterText("AAAAA");

    }
}
