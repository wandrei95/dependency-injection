package com.andrei.wegroszta.depinj.texteditor;

public interface SpellChecker {
    boolean isTextValid(String text);
}
