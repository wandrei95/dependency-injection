package com.andrei.wegroszta.depinj.texteditor;

public class TextEditor {
    private SpellChecker spellChecker;

    public TextEditor() {

    }

    public TextEditor(SpellChecker spellChecker) {
        this.spellChecker = spellChecker;
    }

    public void enterText(String text) {
        if (spellChecker.isTextValid(text)) {
            System.out.println("valid text " + text);
        } else {
            System.out.println("invalid text " + text);
        }
    }
}
